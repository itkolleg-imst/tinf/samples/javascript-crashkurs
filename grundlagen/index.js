/*JS-Grundlagen ES-Grundlagen
*/

let vorname = 'Sarah';
var nachname = 'Musterfrau';
const gender = 'female'

const einArray = ['Ahmet', 'Michael', 'Aurora'];
einArray[3] = 'Seppi';
einArray.push('Maxine');
einArray.sort();


// for (const element of einArray) {
//     console.log(element);
// }

// einArray.forEach(function(element){
//     console.log(element);
// })

einArray.forEach((element) => console.log(element));



function tuWas()
{
    console.log("Ich tue was!");
}
tuWas();

/**
 * Addiert zwei Zahlen miteinander
 * @param {*} x 
 * @param {*} y 
 * @returns Das Ergebnis der Addition
 */
const addiere = function(x,y)
{
    if(isNaN(x) || isNaN(y))
    {
        //Falls Parameter keine Zahlen -> Wirf Fehler
        throw 'Bitte gültige Operanden angeben!';
    }
    return x+y;
}

//Versuche...
try {
    let ergebnis = addiere(5,7)
    // console.log(ergebnis);
//Falls ein Fehler geworfen wird:
} catch (error) {
    console.error("TODO: Fehlerbehandlung implementieren!");
}

/**
 * Subtrahier zwei Zahlen miteinander
 * @param {*} x 
 * @param {*} y 
 * @returns Das Ergebnis der Subtraktion
 */
const subtrahiere = function(x,y)
{
    if(isNaN(x) || isNaN(y))
    {
        throw 'Bitte gültige Operanden angeben!';
    }
    return x-y;
}

// subtrahiere(6,3);


/**
 * Berechnet je nach angegebener Operation ...
 * @param {*} x 
 * @param {*} y 
 * @param {*} operation Die Funktion, die zum Berechnen verwendet wird
 */
function berechne(x,y,operation)
{
    console.log(operation(x,y));
}

//Aufruf mit verschiedenen Funktionen als dritter Parameter
berechne(7,3,subtrahiere);
berechne(7,3,addiere);

