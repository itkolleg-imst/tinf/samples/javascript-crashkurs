/**
 * Async, dh. die Funktion bekommt einen eigenen Thread und läuft unabhängig vom Hauptprogramm ab. Der Aufrufer muss nicht auf die Funktion warten.
 */
async function holeAlleComicHelden()
{
    //Warte auf das Erfüllen des Promieses der fetch-Funktion mit await -> liefert ein Response-Objekt zurück, kein Promise. Kann aber x Sekunden dauern, das verzögert diese Funktion
    let ergebnis = await fetch("https://api.sampleapis.com/futurama/characters");

    //Warte auf das Erfüllen des Promises der json-Funktion. Liefert ein JSON-Objekt zurück. 
    let liste = await ergebnis.json();

    //Hole das <ul> aus dem DOM
    let ulEl = document.querySelector("#futurama");

    //Durchlaufe alle JSON-Objekte in der Liste
    for (const element of liste) {
        // console.log(element.name.first);

        //Erzeuge ein neues <li> und setze den Inhalt
        let liEl = document.createElement("li");
        liEl.textContent = element.name.first + " " + element.name.last;

        //Bild-Element erstellen
        imgEl = document.createElement("img");
        imgEl.alt = element.name.first + " " + element.name.last;
        imgEl.src = element.images.main;

        //Image als Kind von <li> hinzufüfgen
        liEl.appendChild(imgEl);
    
        //<li> als Kind des bestehenden <ul> hinzufügen, damit das Ergebnis auf der Seite sichtbar wird.
        ulEl.appendChild(liEl);
    }
    //
}

//Aufruf der Funktion. Auf das Ergebnis wird nicht gewartet.
holeAlleComicHelden();

//Weiterer Code wird sofort ausgeführt.