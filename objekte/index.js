//JSON: JavaScript Object Notation: Direktes Angeben eines Objekts über dessen Eigenschaften und Werte
const person1 = {
    "vorname": "Michael",
    "nachname": "Gamper",
    "id": 1,
    "features": ["schnell", "klug"]
};


//Definition einer zweiten Person ohne features
const person2 = {
    "vorname": "Maxine",
    "nachname": "Musterfrau",
    "id": 2,
};


//Ändern des Public Datenfeldes
person2.vorname = "Max";


//Definition eines Typs Person als Funktion = vergleichbar mit einer Klasse in Java
const Person = function(vorname, nachname)
{
    //Public-Eigenschaften, aber als private mit _gekennzeichnet
    this._vorname = vorname;
    this._nachname = nachname;
    //Echte private Eigenschaften, da Varialbe nur in der Funktion sichtbar
    let gender = false;

    //Getter-Methode
    this.getVorname = function()
    {
        return this.vorname;
    }
}

//Erzeugen eines Objekts vom Typ Person
const person3 = new Person("Sarah", "Semmel");



/*
 * Definition einer Person mit Hilfe des class-Schlüsselwortes mit Konstruktor und Getter
 */
class Person2 {
    //Private Datenfelder, müssen mit # Beginnen
    #vorname;
    #nachname;
    constructor(vorname, nachname)
    {
        this.#vorname = vorname;
        this.#nachname = nachname;
    }

    get getVorname(){
        return this.#vorname;
    }

}

//Erzeugen einer weiteren Person
person4 = new Person2("Dominik", "Neuner");

//Dynamisches Hinzufügen einer Public-Eigenschaft
person4.titel = "MSc";

//Befüllen eines Arrays mit Personen-Objekten
let personenListe = [person1, person2, person3, person4];

//Durchlaufen des Arrays mit Arrow-Funktion als Parameter der foreach-Methode
personenListe.forEach((element) => console.log(element));
