console.log("Ich laufe");

//Erstelle ein neues DOM-Element..
// console.log(document);

let pEl = document.createElement("p");
pEl.id = "meinAbsatz";
pEl.innerText = "Der neue Inhalt...";
pEl.style = "color: blue;"
console.log(pEl);

// const bodyEl = document.querySelector('body');
// console.log(bodyEl);

// const divEl = document.querySelector('#ergebnis');

// divEl.innerHTML = pEl.outerHTML;
// divEl.appendChild(pEl);

// divEl.innerHTML = `
//     <p>
//         Der neue Inhalt......
//     </p>
// `;


//Hole den Inhalt des input-Elements
let input1El = document.querySelector("#zahl1");
let input2El = document.querySelector("#zahl2");
let buttonEl = document.querySelector("#berechnen");
let ergebnisEl = document.querySelector("#ergebnis");

console.log(buttonEl);

//Handler für den Buttonklick
buttonEl.onclick = addiere;


function addiere()
{
    let ergebnis = Number(input1El.value) + Number(input2El.value);
    let spanEl = document.createElement("span");
    spanEl.innerText = ergebnis;
    // ergebnisEl.appendChild(spanEl);
    ergebnisEl.innerHTML = spanEl.outerHTML;
}


input1El.onkeyup = addiere;
input2El.onkeyup = addiere;