//Definiere etwas HTML als Template-String
const templatestring = `
<link rel="stylesheet" href="Usercard/usercard.css">
<div class="user-card">
  <img />
  <div class="container">
    <h3></h3>
    <div class="info">
    </div>
    <button class="open-modal">Show Info</button>
  </div>
</div>`;

//Erstelle ein neues <template>-Element und befülle es mit dem HTML
const template = document.createElement("template");
template.innerHTML = templatestring;

/**
 * Klasse erbt von HTMLElement und ist damit bereits ein DOM-Element, das alle Methoden und Eigenschaften hat
 */
class Usercard extends HTMLElement{

    //Konstruktor
    constructor()
    {
        super();
        //Erstelle für das Element ein neues Shadow-DOM (noch leer, aber offen für Veränderung)
        this.attachShadow({mode: "open"});
        //Klone die Inhalte des Template-Elements und befülle damit das Shadow-DOM
        this.shadowRoot.appendChild(template.content.cloneNode(true));
    }


    /*
    Wird ausgeführt, sobald das <user-card>-Tag irgendwo verwendet wird, also wenn ein Usercard-Objekt dem DOM hinzugefügt wird
    */
    connectedCallback()
    {
        //Hole die HTML-Attribute aus der <user-card>
        let avatarUrl = this.getAttribute("avatar");
        let characterName = this.getAttribute("name");

        // Hole das <img>-Tag aus dem ShadowDOM und befülle es
        let imgEl = this.shadowRoot.querySelector("img");
        imgEl.src = avatarUrl;
        imgEl.alt = characterName;
        let h3El = this.shadowRoot.querySelector("h3");
        h3El.innerText = characterName;
    }

}

//WICHTIG: Wird benötigt, um dem neuen Custom Element einen Tag-Namen zu geben.
window.customElements.define("user-card", Usercard);

//Nur zu Debugging-Zwecken: Was hat eine UserCard für Eigenschaften? Was steht im shadowroot?
let uc1 = new Usercard();
console.log(uc1);