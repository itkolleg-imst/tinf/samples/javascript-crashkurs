/**
 * Holt alle Futurama-Charaktere von einem WebService und erstellt dynamisch User-cards
 */
async function getAllCharacters()
{
    //Warte auf das Ergebnis und befülle dann die Variable
    let response = await fetch("https://api.sampleapis.com/futurama/characters");
    let json = await response.json();
    // console.log(json);

    //Hole das <main> aus dem Dokument
    let mainEl = document.querySelector("#content");

    //Durchlaufe alle Objekte im Array (alle Charaktere) und erzeuge aus Ihnen <user-card>-Elemente
    for (const element of json) {
        let userCard = new Usercard();
        userCard.setAttribute("avatar", element.images.main);
        userCard.setAttribute("name", element.name.first + " " + element.name.last)
        mainEl.appendChild(userCard);
    }
}

getAllCharacters();
